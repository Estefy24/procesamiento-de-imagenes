#autor: Joana Estefanía Nicolalde Perugachi
#Fecha:2/07/2020
#operaciones aritmeticas de imagenes
#Descripcion: Se desarrolaron las operaciones add,addWeighted,subtract y absdiff
import cv2 as cv
import math


# Leer imagen Simón Bolívar
original = cv.imread("simon_bolivar.jpg")
print("original",original.shape) #Imprime el tamaño

#Convierte a escala de grises
img1 = cv.imread("simon_bolivar.jpg",cv.IMREAD_GRAYSCALE)
#Imagen en blanco y negro
(thresh,blackAndWhiteImage)=cv.threshold(img1,127,255,cv.THRESH_BINARY)
print("Blanco y negro:",blackAndWhiteImage.shape) #imprime el tamaño
print("Imagen 1 gray",img1.shape) #ver tamaño de imagen

# Leer imagen Torre Eiffel en escala de grises
img2 = cv.imread("torre_eiffel.jpg",cv.IMREAD_GRAYSCALE)
#print(img2.shape)


# Realizar operaciones solicitadas
#Primero igualamos las imagenes al mismo tamaño
img1=cv.resize(img1,(512,512))
img2=cv.resize(img2,(512,512))
print("Redimensionandoa la imagen gris",img1.shape) #Ver el tamaño
blackAndWhiteImage=cv.resize(blackAndWhiteImage,(512,512)) #redimensionamos la imagen

#Operacion add
resultadoSuma=cv.add(blackAndWhiteImage,img2)
cv.imshow("Uniendo",resultadoSuma) #mostramos el resultado


#resultadoA=cv.add(img1,img2)
#cv.imshow("Adicion de imagenes",resultadoA)


#alpha y betha para cada una de las imagenes (transparencia), el quitno argumento es para
#que no se sume ningún valor adicional
#resultadoB=cv.addWeighted(img1, 0.5,img2,0.5,0)
#cv.imshow("Mezclar imagenes",resultadoB)


#Sustraccion
#resultadoC=cv.subtract(img1,img2)
#cv.imshow("Sustraccion de imagenes",resultadoC)

resultadoD=cv.absdiff(blackAndWhiteImage,img2)
cv.imshow("Sustraccion de imagenes",resultadoD)

#cv.imshow("Blanco y Negro",blackAndWhiteImage)
#cv.imshow("Original",original)
#cv.imshow("Escala de grises",img1)
cv.waitKey(0)
cv.destroyAllWindows()