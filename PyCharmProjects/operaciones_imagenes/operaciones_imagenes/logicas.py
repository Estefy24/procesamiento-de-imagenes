#autor: Joana Estefanía Nicolalde Perugachi
#Fecha:2/07/2020
#operaciones logicas

#importamos las librerias
import cv2 as cv
import numpy as np

#Leemos la primera imagen
img1=cv.imread("simon_bolivar.jpg")
img1=cv.resize(img1,(800,528)) #Cambia el tamaño
print(img1.shape)

#leemos la segunda imagen
img2=cv.imread("mitad_mundo.jpg")
print(img2.shape)

#Operaciones
#Operacion logica AND
AND=cv.bitwise_and(img1,img2)
#Operacion logica OR
OR =cv.bitwise_or(img1,img2)
#Operacion logica XOR
XOR=cv.bitwise_xor(img1,img2)
#Operacion logica NOT a cada una de las imagenes
NOT1=cv.bitwise_not(img1)
NOT2=cv.bitwise_not(img2)

#Mostramos los resultados
cv.imshow("Funcion Not imagen 1", NOT1)
cv.imshow("Funcion Not imagen 2", NOT2)
cv.imshow("Funcion XOR", XOR)
cv.imshow("Funcion AND", AND)
cv.imshow("Funcion OR", OR)


cv.waitKey(0)
cv.destroyAllWindows()
