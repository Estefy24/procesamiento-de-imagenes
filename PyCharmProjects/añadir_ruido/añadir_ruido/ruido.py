#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 11/09/2020
#Crear Ruido y añadirlo a una imagen
#Descripción: Se describe metodos para la ceación de ruido y filtros de: Ruido Gaussiano, Sal y Pimiento, Poisson, Speckle.
#Importamos las librerías
import numpy as np
import os
import cv2
from matplotlib import pyplot as plt

#Metodo para crear el filtro de Gauss
def noisy(noise_typ,image):
    if noise_typ == "gauss":
     row,col,ch= image.shape
     mean = 0
     var = 0.1
     sigma = var**0.5
     gauss = np.random.normal(mean,sigma,(row,col,ch))
     gauss = gauss.reshape(row,col,ch)
     noisy = image + gauss
     return noisy
    elif noise_typ == "s&p":
     row,col,ch = image.shape
     s_vs_p = 0.5
     amount = 0.004
     out = np.copy(image)
     # Salt mode
     num_salt = np.ceil(amount * image.size * s_vs_p)
     coords = [np.random.randint(0, i - 1, int(num_salt))
       for i in image.shape]
     out[coords] = 255

     # Pepper mode
     num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
     coords = [np.random.randint(0, i - 1, int(num_pepper))
       for i in image.shape]
     out[coords] =255
     return out
    elif noise_typ == "poisson":
     vals = len(np.unique(image))
     vals = 2 ** np.ceil(np.log2(vals))
     noisy = np.random.poisson(image * vals)/float(vals)
     return noisy
    elif noise_typ =="speckle":
     row,col,ch = image.shape
     gauss = np.random.randn(row,col,ch)
     gauss = gauss.reshape(row,col,ch)
     noisy = image + image * gauss
     return noisy

#leemos las imagenes
img1 = cv2.imread("gatohd.jpg")
img = image = cv2.resize(img1,(600,400))
b, g, r = cv2.split(img)
img = cv2.merge([r, g, b])
#Enviamos a los métodos
imgG = noisy("gauss", img)
imgSP = noisy("s&p", img)
imgP = noisy("poisson", img)
imgS = noisy("speckle", img)

cv2.imshow("original",img)
#cv2.imshow("salypimienta",imgSP)

#Mostramos en un plot
plt.subplot(221),plt.imshow(imgG),plt.title('Ruido Gaussiano')
plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(imgSP),plt.title('Sal y Pimienta')
plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(imgP),plt.title('Poisson')
plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(imgS),plt.title('Speckle')
plt.xticks([]), plt.yticks([])
plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()