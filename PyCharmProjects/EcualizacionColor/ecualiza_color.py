#autor: Joana Estefania Nicolalde Perugachi
#fecha: 20/06/2020
#Ecualizar una imagen
#Descripcion: Ecualizando una imagen a color, por los diferentes canales
import cv2 as cv
import os
#Leemos las imagenes
img = cv.imread("imagen_original3.jpg")
ycrcb=cv.cvtColor(img,cv.COLOR_BGR2YCR_CB)
channels=cv.split(ycrcb)
#print(channels)
cv.equalizeHist(channels[0],channels[0])
cv.merge(channels,ycrcb) #Unifica
cv.cvtColor(ycrcb,cv.COLOR_YCR_CB2BGR,img)
#Muestra la imagen real
cv.imshow('Real', img)
#Muestra la imagen ecualizada
cv.imshow('Ecualizada',cv.imread("imagen_original3.jpg"))

#Tiempo de espera
cv.waitKey(0)
cv.destroyAllWindows()