#autor: Joana Estefanía Nicolalde Perugachi
#Fecha:2/07/2020
#Imprimir en un plot, filtros

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('messi1.png',0)
f = np.fft.fft2(img)
fshift = np.fft.fftshift(f)
rows, cols = img.shape
crow,ccol = int(rows/2) , int(cols/2)

dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

# Crea la máscara primero, el centro del cuadrado vale 1, el resto son ceros
mask = np.zeros((rows,cols,2),np.uint8)
mask[crow-30:crow+30, ccol-30:ccol+30] = 1

# Aplica la máscara y la DFT inversa
fshift = dft_shift*mask
f_ishift = np.fft.ifftshift(fshift)
img_back = cv2.idft(f_ishift)
img_back = cv2.magnitude(img_back[:,:,0],img_back[:,:,1])
#cv2.imwrite("nuevomessi.jpg",img_back)

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Imagen de entrada'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img_back, cmap = 'gray')
plt.imsave("nuevomesi.jpg",img_back,cmap = 'gray')
plt.title('DFT'), plt.xticks([]), plt.yticks([])
plt.show()