#autor: Joana Estefanía Nicolalde Perugachi
#Fecha:2/07/2020
#Imprimir en un Plot, filtros, y transformada inversa

#importamos librerías
import cv2
import numpy as np
from matplotlib import pyplot as plt

#leemos la imagen
img = cv2.imread('messi1.png',0)
f = np.fft.fft2(img)
fshift = np.fft.fftshift(f)
#filas y columnas
rows, cols = img.shape
crow,ccol = int(rows/2) , int(cols/2)
fshift[crow-30:crow+30, ccol-30:ccol+30] = 0
f_ishift = np.fft.ifftshift(fshift)
img_back = np.fft.ifft2(f_ishift)
img_back = np.abs(img_back)

#mostramos en el plot
plt.subplot(131),plt.imshow(img, cmap = 'gray')

plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(132),plt.imshow(np.abs(fshift), cmap = 'gray')
plt.imsave("filtro.jpg",np.abs(fshift),cmap = 'gray')
plt.title('Filtro'), plt.xticks([]), plt.yticks([])
plt.subplot(133),plt.imshow(img_back,cmap = 'gray')
plt.imsave("TransformadaInversa.jpg",img_back,cmap = 'gray')
plt.title('Transformada inversa'), plt.xticks([]), plt.yticks([])


plt.show()