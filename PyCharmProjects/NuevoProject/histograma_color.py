#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 26/06/2020
#Histogramas
#Descripcion: Creacion de histogramas a partir de una imagen a color

import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
# img = cv.imread("linda_carter.jfif")
# cv.imshow("linda_carter.jfif", img)
img = cv.imread("bandeMarte-01.jpg")
cv.imshow("bandeMarte-01.jpg", img)
color = ("b","g","r")
#A partir de los diferentes canales
for i, canal in enumerate(color):
    hist = cv.calcHist([img], [i], None, [256], [0, 256])
    plt.plot(hist, color = canal)
    plt.xlim([0,256])
plt.show() #Muestra

cv.destroyAllWindows()