#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 26/06/2020
#Histogramas
#Descripcion: Creacion de histogramas a partir de una imagen en escala de grises
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
img = cv.imread("lena.jfif", cv.IMREAD_GRAYSCALE)
cv.imshow("lena.jfif", img)
histograma = cv.calcHist([img],[0], None, [256],[0,256])

#Muestra en un plot
plt.plot(histograma, color="gray")
plt.xlabel("Intensidad de iluminación")
plt.ylabel("Rango de pixeles")
plt.show()

#Espera un momento
cv.waitKey(0)
cv.destroyAllWindows()