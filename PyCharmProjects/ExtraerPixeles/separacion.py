#autor: Ing. Luis Felipe Borja
#Modificado por: Joana Estefania Nicolalde Perugachi
#fecha:08/09/2020
#Modificado por: Estefanía Nicolalde Perugachi
#Metodo de Benford
#DESCRIPCIÓN: Aplicamos la ley de Benford en una imagen, comprobamos el valor de los pixeles si es que cumplen con esta ley.



#import cv2
#image = cv2.imread("bandera.jpg")
#pixel= image[200, 550]

#print(pixel)

import cv2
import numpy as np
import pandas as pa
import benford as bf
import csv


img = cv2.imread("bande.jpg")
#img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
img=cv2.resize(img, (5,5))
lista =[]
listaB=[]
listaR=[]
listaG=[]
miArray=[]

B = cv2.extractChannel(img, 0)
G = cv2.extractChannel(img, 1)
R = cv2.extractChannel(img, 2)


for i in B:
    lista.append(i)
    listaB.append(i)
    #print(str(i)+" ")

for i in G:
    lista.append(i)
    listaG.append(i)
    #print(str(i)+" ")

for i in R:
    lista.append(i)
    listaR.append(i)
    #print(str(i)+" ")


#toma los valores de toda la lista
#miArray=np.array(lista)
miArray=np.array(listaB)
miArray=miArray.flatten(order='C')
miArray.sort()




print(miArray)


#Escribir un archivo csv


#myData = [["first_name", "second_name", "Grade"],
 #         ['Alex', 'Brian', 'A'],
  #        ['Tom', 'Smith', 'B']]

myFile = open('MisDatosB.csv', 'w')
with myFile:
    writer = csv.writer(myFile)
    writer.writerows(listaB)

print("Writing complete")


#sp = pa.read_csv('MisDatosB.csv')
#f1d = bf.first_digits(sp.l_r, digs=1, decimals=8) # digs=1 for the first digit (1-9)

#print(f1d)






#matrix flat python #convierte en una lista los valores de una matriz

#Imprimir sin salto de linea
#print(R ,end="")
#print(G,end="")
#print(B,end="")



#Para imprimir pixeles

#hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
#p = img.shape
#print(p)

#img2=img
#img2=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#rows,cols = img.shape

#for i in range(rows):
    #for j in range(cols):
        #k = img[i,j]
        #print(" " + str(k),end="")


    #print(" ")

