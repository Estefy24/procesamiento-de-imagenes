#autor: Ing. Luis Felipe Borja
#Modificado por: Joana Estefania Nicolalde Perugachi
#fecha:08/09/2020
#Modificado por: Estefanía Nicolalde
#Metodo de Benford
#DESCRIPCIÓN: Aplicamos la ley de Benford en una imagen, comprobamos el valor de los pixeles si es que cumplen con esta ley.

from __future__ import division
from itertools import islice, count
from collections import Counter
from math import log10
from random import randint

#Importamos las librerios
import cv2
import numpy as np
import pandas as pa
import benford as bf
import csv

#leemos la imagen
img = cv2.imread("foto3.jpg")
#img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
img=cv2.resize(img, (5,5))#redimensionamos la imagen
#creamos nuevas listas
lista =[]
listaB=[]
listaR=[]
listaG=[]
miArray=[]

#Extracción de los canales
B = cv2.extractChannel(img, 0)
G = cv2.extractChannel(img, 1)
R = cv2.extractChannel(img, 2)

#Guardamos en las listas los diferentes valores de los pixeles generados para cada canal
for i in B:
    lista.append(i)#agrega a la lista
    listaB.append(i)
    #print(str(i)+" ")

for i in G:
    lista.append(i)#agrega a la lista
    listaG.append(i)
    #print(str(i)+" ")

for i in R:
    lista.append(i)#agrega a la lista
    listaR.append(i)
    #print(str(i)+" ")


#toma los valores de toda la lista
#miArray=np.array(lista)
#miArray=np.array(listaB)
miArray=np.array(lista)
#miArray=np.array(listaR)
miArray=miArray.flatten(order='C')
miArray.sort()



#imprimimos el arreglo
print(miArray)





expected = [log10(1 + 1 / d) for d in range(1, 10)]


def fib():
    a, b = 1, 1
    while True:
        yield a
        a, b = b, a + b


# powers of 3 as a test sequence
def power_of_threes():
    return (3 ** k for k in count(0))


def heads(s):
    for a in s: yield int(str(a)[0])


def show_dist(title, s):
    c = Counter(s)
    size = sum(c.values())
    res = [c[d] / size for d in range(1, 10)]

    print("\n%s Benfords deviation" % title)
    for r, e in zip(res, expected):
        print("%5.1f%% %5.1f%%  %5.1f%%" % (r * 100., e * 100., abs(r - e) * 100.))


def intento():
    i = 0
    while True and i<len(miArray):
        yield miArray[i] #randint(1, 9999)
        i=i+1


def rand1000():

    while True :
        yield randint(1, 9999)

#Main, llamamos los metodos para la impresion de los resultados
if __name__ == '__main__':
    show_dist("fibbed", islice(heads(fib()), len(miArray)))
    show_dist("threes", islice(heads(power_of_threes()), len(miArray)))

    # just to show that not all kind-of-random sets behave like that
    show_dist("random", islice(heads(rand1000()), len(miArray)))
    show_dist("MatrizB", islice(heads(intento()), len(miArray)))