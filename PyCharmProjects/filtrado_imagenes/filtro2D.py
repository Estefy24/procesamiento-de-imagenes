#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 02/07/2020
#Implementacion de filtros
#Descripcion: Se implemento el filtro filter2D para imagenes con ruido

#importamos librerias
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('img_ruido_pimienta.jpg')
#Crea el kernel 5x5 / 25
kernel = np.ones((5,5),np.float32)/25
#Filtra la imagen utilizando el kernel anterior
dst = cv.filter2D(img,-1,kernel)


#Filtro Speckles
#imagen3=cv.filterSpeckles(img,0.5,1,0.7,None)
#cv.imshow("Imagen Nueva",imagen3)


#Filtramos una vez más
imagen2 = cv.filter2D(dst,-1,kernel)
#cv.imshow("Filtro 2 veces ",imagen2)
imagen3 = cv.filter2D(imagen2,-1,kernel)

#cv.imshow("Filtro 3 veces ",imagen3)


#Para mostrar las imagenes en un plot
plt.subplot(121),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(dst),plt.title('Promediada')
plt.xticks([]), plt.yticks([])
plt.show()



#Muestra otro plot
plt.subplot(121),plt.imshow(imagen2),plt.title('Promediada 2')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(imagen3),plt.title('Promediada 3')
plt.xticks([]), plt.yticks([])
# plt.subplot(122),plt.imshow(imagen2),plt.title('Promediada_2')
# plt.xticks([]), plt.yticks([])
plt.show()



