#autora: Joana Estefania Nicolalde Perugachi
#fecha: 23/07/2020
#Ejercicios de Dilatacion

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('uno.jpg')

b,g,r= cv2.split(img)
img=cv2.merge([r,g,b])


#elemento estructurante
#vamos a usar un elemento estructurante cuadrado de 3x3
se1 = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
#kernel = np.ones((3,3),np.uint8)

dilatacion = cv2.morphologyEx(img, cv2.MORPH_DILATE, se1)


dilatacion2 = cv2.morphologyEx(dilatacion, cv2.MORPH_DILATE, se1)
dilatacion3 = cv2.morphologyEx(dilatacion2, cv2.MORPH_DILATE, se1)

#Deteccion de Bordes

salida=dilatacion-img
salida=cv2.resize(salida,(400,400))
cv2.imshow('Dilatacion con una imagen estructurante', salida)


#Dilatacion

plt.subplot(221),plt.imshow(img)
plt.title('Imagen original')
plt.xticks([]), plt.yticks([])

plt.subplot(222),plt.imshow(dilatacion)
plt.title('Imagen Dilatada 1')
plt.xticks([]), plt.yticks([])

plt.subplot(223),plt.imshow(dilatacion2)
plt.title('Imagen Dilatada 2')
plt.xticks([]), plt.yticks([])


plt.subplot(224),plt.imshow(dilatacion3)
plt.title('Imagen Dilatada 3')
plt.xticks([]), plt.yticks([])


plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()
