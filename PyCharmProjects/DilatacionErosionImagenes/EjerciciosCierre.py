#autora: Joana Estefania Nicolalde Perugachi
#fecha: 23/07/2020
#Ejercicios de Cierre

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('c1.jpg')

#img=cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

#imagen binaria blanco y negro

#img_g = cv2.imread('uno.jpg', cv2.IMREAD_GRAYSCALE)
#(thresh, img) = cv2.threshold(img_g, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)




#elemento estructurante
#vamos a usar un elemento estructurante cuadrado de 3x3
se1 = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
#kernel = np.ones((3,3),np.uint8)



cierre = cv2.morphologyEx(img, cv2.MORPH_CLOSE, se1)


cierre2 = cv2.morphologyEx(cierre, cv2.MORPH_CLOSE, se1)
cierre3 = cv2.morphologyEx(cierre2, cv2.MORPH_CLOSE, se1)

#Deteccion de Bordes

salida=cierre-img
salida=cv2.resize(salida,(400,400))
cv2.imshow('Cierre con una imagen estructurante', salida)


b,g,r= cv2.split(img)
img=cv2.merge([r,g,b])

#cierre

plt.subplot(221),plt.imshow(img)
plt.title('Imagen cierre')
plt.xticks([]), plt.yticks([])

plt.subplot(222),plt.imshow(cierre)
plt.title('Imagen cierre 1')
plt.xticks([]), plt.yticks([])

plt.subplot(223),plt.imshow(cierre2)
plt.title('Imagen cierre 2')
plt.xticks([]), plt.yticks([])


plt.subplot(224),plt.imshow(cierre3)
plt.title('Imagen cierre 3')
plt.xticks([]), plt.yticks([])


plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()
