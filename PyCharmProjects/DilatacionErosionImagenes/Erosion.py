#autora: Joana Estefania Nicolalde Perugachi
#fecha: 23/07/2020
#Erosion, diltacion, apertura, cierre y gradiente en una imagen
#Descripcion: Se desarrollo los filtros de erosion, diltacion, apertura, cierre y gradiente en una imagen
#para lo cual se uso el kernel

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('j.png')

b,g,r= cv2.split(img)
img=cv2.merge([r,g,b])


#Elemento Estructurante

#cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))
#cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))

cv2.getStructuringElement(cv2.MORPH_CROSS, (5,5))

kernel = np.ones((7,7),np.uint8)
#Erosion
erosion = cv2.erode(img,kernel,iterations = 1)
#Dilatacion
dilatacion=cv2.dilate(img,kernel,iterations=1)
#Apertura
apertura=cv2.morphologyEx(img,cv2.MORPH_OPEN,kernel)
#Cierre
cierre=cv2.morphologyEx(img,cv2.MORPH_CLOSE,kernel)
#Gradiente
gradiente=cv2.morphologyEx(img,cv2.MORPH_GRADIENT,kernel)

#Sombrero de Copa
sombrero_copa=cv2. morphologyEx(img,cv2.MORPH_TOPHAT,kernel)
#cv2.imshow("imagen original",img)

#Sombrero negro
sombrero_negro=cv2. morphologyEx(img,cv2.MORPH_BLACKHAT,kernel)

#cv2.imshow("imagen original",img)


plt.subplot(241),plt.imshow(img)
plt.title('Imagen original')
plt.xticks([]), plt.yticks([])


plt.subplot(242),plt.imshow(erosion)
plt.title('Erosion')
plt.xticks([]), plt.yticks([])

plt.subplot(243),plt.imshow(dilatacion)
plt.title('Dilatacion')
plt.xticks([]), plt.yticks([])

plt.subplot(244),plt.imshow(apertura)
plt.title('Apertura')
plt.xticks([]), plt.yticks([])


plt.subplot(245),plt.imshow(cierre)
plt.title('Cierre')
plt.xticks([]), plt.yticks([])

plt.subplot(246),plt.imshow(gradiente)
plt.title('Gradiente')
plt.xticks([]), plt.yticks([])


plt.subplot(247),plt.imshow(sombrero_copa)
plt.title('Sombrero de Copa')
plt.xticks([]), plt.yticks([])

plt.subplot(248),plt.imshow(sombrero_negro)
plt.title('Sombrero Negro')
plt.xticks([]), plt.yticks([])



plt.show()



cv2.waitKey(0)
cv2.destroyAllWindows()
