#autora: Joana Estefania Nicolalde Perugachi
#fecha: 23/07/2020
#Ejercicios de Apertura

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('uno.jpg')

b,g,r= cv2.split(img)
img=cv2.merge([r,g,b])


#elemento estructurante
#vamos a usar un elemento estructurante cuadrado de 3x3
#elimina las lineas para la a1 y las va convirtiendo en c
se1 = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
#para la imagen a2 con un elemento estructurante de 3x9 (lineas separadas horizontales y verticales)}
#para horizontal 9x3
se2 = cv2.getStructuringElement(cv2.MORPH_RECT, (3,9)) #vertical
se2_ = cv2.getStructuringElement(cv2.MORPH_RECT, (9,3)) #horizontal
#para la imagen mono con un elemento estructurante de 5x5 (mono)

se3 = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))

#kernel = np.ones((3,3),np.uint8)


#crea la apertura
apertura = cv2.morphologyEx(img, cv2.MORPH_OPEN, se1)

#aplicamos mas aperturas
apertura2 = cv2.morphologyEx(apertura, cv2.MORPH_OPEN, se1)
apertura3 = cv2.morphologyEx(apertura2, cv2.MORPH_OPEN, se1)

#Deteccion de Bordes

salida=apertura-img
salida=cv2.resize(salida,(400,400))
cv2.imshow('Apertura con una imagen estructurante', salida)


plt.subplot(221),plt.imshow(img)
plt.title('Imagen original')
plt.xticks([]), plt.yticks([])

plt.subplot(222),plt.imshow(apertura)
plt.title('Imagen apertura 1')
plt.xticks([]), plt.yticks([])

plt.subplot(223),plt.imshow(apertura2)
plt.title('Imagen apertura 2')
plt.xticks([]), plt.yticks([])


plt.subplot(224),plt.imshow(apertura3)
plt.title('Imagen apertura 3')
plt.xticks([]), plt.yticks([])


plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()
