#autor: Joana Estefanía Nicolalde Perugachi
#Fecha:2/07/2020

#importamos las librerias
import cv2
import numpy as np
from matplotlib import pyplot as plt
image1 = cv2.imread("bolas_colores.png") #Leemos la imagen
image = cv2.resize(image1,(600,400)) #Redimensionamos el tamaño
hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) # Convertir BGR to HSV
# definir rango de colores
azul_bajos = np.array([100,50,50])
azul_altos = np.array([130, 255, 255])
amarillo_bajos = np.array([20,50,100])
amarillo_altos = np.array([40, 255, 255])
verde_altos = np.array([70,255,255])
verde_bajos = np.array([50,50,50])
#rojo requiere de 2 rangos diferentes
rojo_altos1 = np.array([179,255,255])
rojo_bajos1 = np.array([170,100,100])
rojo_bajos2 = np.array([0,100,100])
rojo_altos2 = np.array([10,255,255])
# Umbral de la imagen HSV para obtener colores
mask_amarillos = cv2.inRange(hsv, amarillo_bajos, amarillo_altos)
mask_verde = cv2.inRange(hsv, verde_bajos, verde_altos)
mask_azul = cv2.inRange(hsv, azul_bajos, azul_altos)
#2 mascaras para el rojo
mask_rojo1 = cv2.inRange(hsv, rojo_bajos1, rojo_altos1)
mask_rojo2 = cv2.inRange(hsv, rojo_bajos2, rojo_altos2)
#combinamos las 2 mascaras de rojo
mask_rojo  = cv2.bitwise_or(mask_rojo1, mask_rojo2)
cv2.imshow("imagen original",image) #Muestra la imagen original
#Muestra las diferentes mascaras
plt.subplot(221),plt.imshow(mask_amarillos),plt.title('mascara amarillos')
plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(mask_verde),plt.title('mascara verde')
plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(mask_rojo),plt.title('mascara rojo')
plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(mask_azul),plt.title('mascara azul')
plt.xticks([]), plt.yticks([])
plt.show() #Muestra los
cv2.destroyAllWindows()