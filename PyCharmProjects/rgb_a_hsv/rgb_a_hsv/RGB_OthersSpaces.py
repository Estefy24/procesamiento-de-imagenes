#autor: Joana Estefanía Nicolalde Perugachi
#Fecha:2/07/2020
#Conversion de Espacios
import cv2 as cv
import numpy as np
#Leemos la imagen
img1=cv.imread("imagen1.png")
#Redimensionamos la imagen
img1 = cv.resize(img1,(300,200))

#En OpenCV RGB se llama BGR, por el orden en el que se
# guardan cada canal.

#Cambiamos de espacio de color de RGB a  HSV
img_hsv=cv.cvtColor(img1,cv.COLOR_BGR2HSV)
#Redimensionamos la imagen
img_hsv = cv.resize(img_hsv,(300,200))

#Muestra en una sola fila ambas imagenes
cv.imshow("BGR-HSV",np.hstack((img1,img_hsv)))




#Cambiamos de espacio de color de RGB a  XYZ
img_xyz = cv.cvtColor(img1,cv.COLOR_BGR2XYZ)
img_xyz = cv.resize(img_xyz,(300,200))

#cv.imshow("BGR_XYZ",np.hstack((img1,img_xyz)))


#Cambiamos de espacio de color de RGB a  Luv
img_luv = cv.cvtColor(img1,cv.COLOR_BGR2Luv)
img_luv = cv.resize(img_luv,(300,200))

cv.imshow("BGR_Luv",np.hstack((img1,img_luv)))


#Cambiamos de espacio de color de RGB a  Lab
img_lab = cv.cvtColor(img1,cv.COLOR_BGR2Lab)
img_lab = cv.resize(img_lab,(300,200))

cv.imshow("BGR_Lab",np.hstack((img1,img_lab)))





cv.waitKey(0)
cv.destroyAllWindows()


#cv.imshow("Imagen Original BGR ",img1)
#cv.imshow("Imagen Transformada ",img_hsi)
