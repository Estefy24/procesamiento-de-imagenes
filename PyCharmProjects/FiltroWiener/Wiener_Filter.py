#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 03/08/2020
#Referencia: https://sites.google.com/view/leanhtran
#Modificación en el código de algunos filtros tales como Blur, Wiewer, ruido gaussiano


import os
import numpy as np
from numpy.fft import fft2, ifft2
from scipy.signal import gaussian, convolve2d
import matplotlib.pyplot as plt

#Efecto Blur
def blur(img, kernel_size = 3):
	dummy = np.copy(img) #hace una copia de la imagen
	h = np.eye(kernel_size) / kernel_size
	dummy = convolve2d(dummy, h, mode = 'valid') #convolucion
	return dummy

#añadir ruido gausiano
def add_gaussian_noise(img, sigma):
	gauss = np.random.normal(0, sigma, np.shape(img))
	noisy_img = img + gauss  #operacion de suma de imagenes
	noisy_img[noisy_img < 0] = 0
	noisy_img[noisy_img > 255] = 255
	return noisy_img

#aplica filtro Wiener
def wiener_filter(img, kernel, K):
	kernel /= np.sum(kernel) #kernel
	dummy = np.copy(img) #crear una copia de la imagen
	dummy = fft2(dummy)
	kernel = fft2(kernel, s = img.shape)
	kernel = np.conj(kernel) / (np.abs(kernel) ** 2 + K)
	dummy = dummy * kernel
	dummy = np.abs(ifft2(dummy))
	return dummy

#kernel gausiano
def gaussian_kernel(kernel_size = 3):
	h = gaussian(kernel_size, kernel_size / 3).reshape(kernel_size, 1)
	h = np.dot(h, h.transpose())
	h /= np.sum(h)
	return h

#metodo para pasar de rgb a escala de grises
def rgb2gray(rgb):
	return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])

#Main
if __name__ == '__main__':
	# Load image and convert it to gray scale
	file_name = os.path.join('ciudad.jpg') #lee la imagen con os
	img = rgb2gray(plt.imread(file_name)) #convierte la imagen a escala de grises y la lee con la libreria plot

	# Blur para la imagen, envia sus diferentes parametros
	blurred_img = blur(img, kernel_size = 15)

	# Imagen con ruido gausiano
	noisy_img = add_gaussian_noise(blurred_img, sigma = 20)

	# Imagen aplicando el filtro Wiener
	kernel = gaussian_kernel(3)
	filtered_img = wiener_filter(noisy_img, kernel, K = 10)

	# Mostramos los resultadoss en un plot
	display = [img, blurred_img, noisy_img, filtered_img]#crea un arreglo de las imagenes generadas


	#un arreglo para los titulos
	label = ['Imagen Original', 'Blured de movimiento', 'Blur y Ruido Gaussiano', 'Filtro Wiener']

	fig = plt.figure(figsize=(12, 10))#tamaño para las imagenes

	for i in range(len(display)):
		fig.add_subplot(2, 2, i+1) #añade la imagen en cierta posicion
		plt.imshow(display[i], cmap = 'gray') #muestra en escala de grises
		plt.title(label[i]) #añade el titulo
		plt.imsave(label[i] + ".jpg", display[i], cmap='gray') #guardamos las imagenes

	plt.show() #mostramos el plot
