#Autor: Joana Estefania Nicolalde Perugachi
#Fecha: 16/06/2020
#Mi hola mundo en OpenCv
#Descripcion: Leer una imagen y visualizarla en OpenCv

#importar libreria opencv
import cv2 as cv
#leer imagen
imagen = cv.imread(".\Imagenes\gato.jpg")
#visualizar la imagen
cv.imshow("mi gato",imagen)
#esperar tecla
cv.waitKey(0)
