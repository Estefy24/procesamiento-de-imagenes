#Autor: Joana Estefania Nicolalde Perugachi
#Fecha: 16/06/2020
#Direccionamiento de imagenes
#Descripcion: Leer una imagen y visualizarla en OpenCv
# y abrirlas de los diferentes directorios y hacerla a escala de grises

import cv2 as cv
import sys
#leer directorios externos
hada = cv.imread("..\MiImagen\hada.jpg", cv.IMREAD_GRAYSCALE)
#hada=cv.imread(cv.samples.findFile("D:/MisDocs/ClasesProcesamiento/MiImagen/hada1.jpg"))

if hada is None:
     #print("hola")
     sys.exit("No se puede leer la imagen")

cv.imshow("mi hada", hada)
cv.imwrite("..\MiImagen\hada_gris.jpg",hada)


# cargar el archivo PNG indicado
img = cv.imread(".\Imagenes\gato.jpg", cv.IMREAD_GRAYSCALE)
# mostrar la imagen en una ventana
cv.imshow('Mi gato sin color', img)

imagen = cv.imread(".\Imagenes\gato.jpg")
#visualizar la imagen
cv.imshow("mi gato con color",imagen)



# guardar la imagen en formato JPG
cv.imwrite(".\Imagenes\gatoGris.jpg", img)


# esperar hasta que se presiona una tecla
cv.waitKey(0)

#Liberar memoria
cv.destroyAllWindows()

