#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 26/06/2020
#Ecualizacion
#Descripcion: Proceso para mostrar una imagen original y su ecualizacion, histogramas en una sola ventana

#importamos las librerias
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

from skimage import io

#Histograma Color
img2 = cv.imread("input.png")

ycrcb=cv.cvtColor(img2,cv.COLOR_BGR2YCR_CB)
channels=cv.split(ycrcb)
#print(channels)
img2_eql=cv.equalizeHist(channels[0],channels[0])
img2_eql=cv.merge(channels,ycrcb)
img2_eql=cv.cvtColor(ycrcb,cv.COLOR_YCR_CB2BGR,img2_eql) #devuelve al color original
# guardar la imagen en formato JPG
cv.imwrite(".\gNuevaChica.jpg", img2_eql)
#cv.imshow("",np.hstack((img2,img2_eql))) #Muestra en una sola fila

#Histogramas de las imagenes

histograma_img2_or=cv.calcHist([img2],[0], None, [256],[0,256])
histograma_img2_eq=cv.calcHist([img2_eql],[0], None, [256],[0,256])

#Una sola ventana 2x2
#fila columna lugar
plt.subplot(2,2,1),plt.imshow(io.imread("input.png")/255.00)
plt.title('Original'), plt.xticks([]), plt.yticks([])
#
#
plt.subplot(2,2,2),plt.imshow(io.imread("gNuevaChica.jpg")/255.00)
plt.title('Equalizacion'), plt.xticks([]), plt.yticks([])


plt.subplot(2,2,3),plt.plot(histograma_img2_or) #color="gray")
plt.title('Histograma Original imagen 2'),plt.xlabel("Intensidad de Iluminacion"), plt.ylabel("Rango de pixeles")

plt.subplot(2,2,4),plt.plot(histograma_img2_eq) #, color="gray")
plt.title('Histograma Equalizado imagen 2'),plt.xlabel("Intensidad de Iluminacion"), plt.ylabel("Rango de pixeles")

plt.show()



img = cv.imread("imagen_original.png")
histograma_or=cv.calcHist([img],[0], None, [256],[0,256])

#img_to_eql = cv.cvtColor(img,cv.COLOR_BGR2RGB)
img_to_eql = cv.cvtColor(img,cv.COLOR_BGR2YUV)
img_to_eql[:,:,0] = cv.equalizeHist(img_to_eql[:,:,0])
hist_eql_resultado = cv.cvtColor(img_to_eql,cv.COLOR_YUV2BGR)
histograma_eq=cv.calcHist([hist_eql_resultado],[0], None, [256],[0,256])




#cv.imshow('Ecualizada',img_ecualizada)


#cv.imshow("Original",cv.imread("imagen_original3.jpg") )





# # #Histograma Original
# plt.plot(histograma_or, color="gray")
# #plt.savefig("histograma.jpg") # la guarda como imagen
# plt.xlabel("Intensidad de iluminacion")
# plt.ylabel("Rango de pixeles")
# #plt.title('Histograma original')
# plt.show()
#
# plt.plot(histograma_eq, color="gray")
# plt.xlabel("Intensidad de iluminacion")
# plt.ylabel("Rango de pixeles")
# #plt.title('Histograma equalizado')
# plt.show()
#plt.title('histograma Original'), plt.xticks([]), plt.yticks([])

# #Histograma equalizado
#






# cv.imshow("imagen original",img)
# cv.imshow("imagen resultado",hist_eql_resultado)

#cv.imshow("",np.hstack((img,hist_eql_resultado)))



#Mostrar todas en una sola ventana 3x2

#
plt.subplot(2,4,1),plt.imshow(img)
plt.title('Original'), plt.xticks([]), plt.yticks([])
#
#
plt.subplot(2,4,2),plt.imshow(hist_eql_resultado)
plt.title('Equalizacion'), plt.xticks([10]), plt.yticks([10])

plt.subplot(2,4,3),plt.imshow(cv.imread("imagen_original3.jpg"))
plt.title('Imagen Real'),plt.xticks([10]), plt.yticks([10])


plt.subplot(2,4,5),plt.plot(histograma_or, color="gray")
plt.title('Histograma Original'),plt.xlabel("Intensidad de Iluminacion"), plt.ylabel("Rango de pixeles")

plt.subplot(2,4,6),plt.plot(histograma_eq, color="gray")
plt.title('Histograma Equalizado'),plt.xlabel("Intensidad de Iluminacion"), plt.ylabel("Rango de pixeles")

plt.subplot(2,4,7),plt.imshow(img2_eql)
plt.title('Histograma Equalizado Color'),plt.xlabel("Intensidad de Iluminacion"), plt.ylabel("Rango de pixeles")

#plt.show()


cv.waitKey(0)
cv.destroyAllWindows()

#cv.imwrite("imagen_resultado.png",hist_eql_resultado)

