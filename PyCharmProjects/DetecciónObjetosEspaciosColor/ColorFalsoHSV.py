#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 11/07/2020
#Detección de Colores Falsos
#Descripción: Para detectar una imagen falsa en una imagen usamos la función applyColorMap

import cv2 as cv
import numpy as np
image_bw = cv.imread("TfBmw.jpg", cv.IMREAD_GRAYSCALE)
image_rgb = cv.imread("TfBmw.jpg")

image_cm = cv.applyColorMap(image_rgb, cv.COLORMAP_JET)
cv.imshow("Nueva_imagen" ,image_cm)
cv.imshow("Imagen Original",image_rgb)

# #create the image arrays we require for the processing
#hue=np.zeros((400,300), dtype=np.uint8)
#sat=np.zeros((400,300), dtype=np.uint8)
#val=np.zeros((400,300), dtype=np.uint8)
#mask1=np.zeros((400,300), dtype=np.uint8)
#mask2=np.zeros((400,300), dtype=np.uint8)


# #convert to cylindrical HSV color space
#cv.cvtColor(image_rgb,cv.COLOR_BGR2HSV) #como lee en bgr
# #split image into component channels
# cv.split(image_rgb,hue,sat,val,None)
# #rescale image_bw to degrees
# cv.ConvertScale(image_bw, image_bw, 180 / 256.0)
# #set the hue channel to the greyscale image
# cv.Copy(image_bw,hue)
# #set sat and val to maximum
# cv.Set(sat, 255)
# cv.Set(val, 255)
#
# #adjust the pseudo color scaling offset, 120 matches the image you displayed
# offset=120
# cv.CmpS(hue,180-offset, mask_1, cv.CV_CMP_GE)
# cv.CmpS(hue,180-offset, mask_2, cv.CV_CMP_LT)
# cv.AddS(hue,offset-180,hue,mask_1)
# cv.AddS(hue,offset,hue,mask_2)
#
# #merge the channels back
# cv.Merge(hue,sat,val,None,image_rgb)
# #convert back to RGB color space, for correct display
# cv.CvtColor(image_rgb,image_rgb,cv.CV_HSV2RGB)
#
# cv.ShowImage('image', image_rgb)
# # cv.SaveImage('TfBmw_120.jpg',image_rgb)

#Para detectar colores de RGB en HSV

#green = np.uint8([[[157,86,66 ]]])

#hsv_green = cv.cvtColor(green,cv.COLOR_BGR2HSV)

#print (hsv_green)



cv.waitKey(0)
cv.destroyAllWindows()

