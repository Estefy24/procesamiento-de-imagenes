#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha:10/07/2020
#Aplicacion del espacio de color hsv
#SEGUIMIENTO DE OBJETOS
#Aplicando mascaras podemos detectar objetos dentro de una imagen, esto a traves de los colores

#importamos las librerias
import cv2
import numpy as np
from matplotlib import pyplot as plt

#Leemos la imagen con la que vamos a trabajar
image = cv2.imread('perrito.jpg')

#Convertimos del espacio BGR (Como lo lee OPenCV a RGB) a HSV
hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

#Definimos el rango de color azul en HSV
lower_verde = np.array([25,50,50])
upper_verde = np.array([67,255,255])
lower_rosa1 = np.array([125,50,50])
upper_rosa1 = np.array([167,255,255])

lower_crema = np.array([0,0, 100])
upper_crema = np.array([25,180,255])

lower_crema2 = np.array([170 ,200 ,100])
upper_crema2 = np.array([180, 255, 255])

lower_cremaoscur = np.array([170 ,0 ,100])
upper_cremaoscuro = np.array([180, 180, 255])

lower_cremaoscur2 = np.array([170 ,0 ,15])
upper_cremaoscuro2 = np.array([180, 255,250])

#Damos los rangos para las mascaras
mask = cv2.inRange(hsv, lower_verde, upper_verde) #mascara para la pelota
mask1 = cv2.inRange(hsv, lower_rosa1, upper_rosa1) #mascara para la camiseta
mask2=cv2.inRange(hsv, lower_crema, upper_crema)
mask3=cv2.inRange(hsv, lower_crema2, upper_crema2)
mask4=cv2.inRange(hsv, lower_cremaoscur, upper_cremaoscuro)
mask5=cv2.inRange(hsv, lower_cremaoscur2, upper_cremaoscuro2)

#cv2.imshow("mascara bola",mask)
#cv2.imshow("mascara_camiseta",mask1)
#cv2.imshow("mascara_nueva",mask2)
#cv2.imshow("mascara4",mask4)

#Aplicamos las operaciones bitwise y and para realizar las mascaras
bola = cv2.bitwise_and(image,image, mask= mask)
camiseta = cv2.bitwise_and(image,image, mask= mask1)
piel1=cv2.bitwise_and(image,image, mask= mask2)
piel2=cv2.bitwise_and(image,image, mask= mask3)
piel3=cv2.bitwise_and(image,image, mask= mask4)
piel4=cv2.bitwise_and(image,image, mask= mask5)
pielj=cv2.bitwise_or(piel1,piel3)
pielk=cv2.bitwise_or(pielj,piel2)
piel=cv2.bitwise_or(pielk,piel4)

#cv2.imshow("piel",piel)

#Guardamos las imagenes resultantes
cv2.imwrite(".\mascara.jpg", mask)
cv2.imwrite(".\mascara1.jpg", mask1)
cv2.imwrite(".\labola.jpg", bola)
cv2.imwrite(".\camiseta.jpg", camiseta)
cv2.imwrite(".\piel.jpg", piel)

#Mostramos los resultados, para ello leemos las nuevas imagenes que las pasaremos a nuestro espacio rgb
image_ = cv2.imread('perrito.jpg')
mask_ = cv2.imread('mascara.jpg')
mask1_ = cv2.imread('mascara1.jpg')
bola_ = cv2.imread('labola.jpg')
camiseta_ = cv2.imread('camiseta.jpg')
piel_ = cv2.imread('piel.jpg')

#Devolvemos a los canales originales
b,g,r= cv2.split(image_)
image_=cv2.merge([r,g,b])

b,g,r= cv2.split(mask_)
mask_=cv2.merge([r,g,b])

b,g,r= cv2.split(mask1_)
mask1_=cv2.merge([r,g,b])

b,g,r= cv2.split(bola_)
bola_=cv2.merge([r,g,b])

b,g,r= cv2.split(camiseta_)
camiseta_=cv2.merge([r,g,b])

b,g,r= cv2.split(piel_)
piel_=cv2.merge([r,g,b])

#Finalmente mostramos las imagenes en su espacio original
plt.subplot(231),plt.imshow(image_)
plt.title('Imagen original')
plt.xticks([]), plt.yticks([])
#

plt.subplot(232),plt.imshow(mask1_)
plt.xticks([]), plt.yticks([])
plt.title('Mascara_camiseta')


plt.subplot(233),plt.imshow(mask_)
plt.xticks([]), plt.yticks([])
plt.title('Mascara_pelota')


plt.subplot(234),plt.imshow(piel_)
plt.xticks([]), plt.yticks([])
plt.title('piel')

plt.subplot(235),plt.imshow(camiseta_)
plt.xticks([]), plt.yticks([])
plt.title('camiseta')

plt.subplot(236),plt.imshow(bola_)
plt.xticks([]), plt.yticks([])
plt.title('Bola')


plt.show()



cv2.waitKey(0)
cv2.destroyAllWindows()
